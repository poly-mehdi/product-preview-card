const cardImg = document.querySelector('.card-img')

function updateImageSource() {
  const desktopSrc = cardImg.dataset.srcDesktop
  const mobileSrc = cardImg.dataset.srcMobile

  if (window.innerWidth >= 768) {
    cardImg.src = desktopSrc
  } else {
    cardImg.src = mobileSrc
  }
}

// Mettre à jour la source de l'image initialement
updateImageSource()

// Écouter le changement de la fenêtre pour mettre à jour la source de l'image en temps réel
window.addEventListener('resize', updateImageSource)
